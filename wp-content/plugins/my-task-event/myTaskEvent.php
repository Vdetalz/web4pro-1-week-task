<?php
/*
Plugin Name: myTaskEvent
Description: плагин событий.
Version: 1.0
Author: Vitaliy
*/

/**
 * add new taxonomy
 */
add_action( 'init', 'my_events_taxonomy' );
function my_events_taxonomy() {
	$labels = array(
		'name'              => __( 'Категории Событий' ),
		'singular_name'     => __( 'Категория Событий' ),
		'search_items'      => __( 'Найти Категорию событий' ),
		'all_items'         => __( 'Все' ),
		'parent_item'       => __( 'Родительская Категория Событий' ),
		'parent_item_colon' => __( 'Родительская Категория Событий:' ),
		'edit_item'         => __( 'Редактировать Категорию событи' ),
		'view item'         => __( 'Посмотреть Категорию событий' ),
		'update_item'       => __( 'Обновить Категорию событий' ),
		'add_new_item'      => __( 'Добавить Категорию событий' ),
		'new_item_name'     => __( 'Новое Имя Категории' ),
		'menu_name'         => __( 'Категории Событий' )
	);
	$args   = array(
		'label'             => __( 'Категории Событий' ),
		'labels'            => $labels,
		'public'            => true,
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_in_menu'      => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array(
			'slug' => '' // Текст в ЧПУ. По умолчанию: название таксономии.
			/*'with_front' => false, // Позволяет ссылку добавить к базовому URL.
			'hierarchical' => true, // Использовать (true) или не использовать (false) древовидную структуру ссылок. По умолчанию: false.
			'ep_mask' => EP_NONE // Перезаписывает конечное значение таксономии. По умолчанию: EP_NONE.
		*/
		),
	);
	register_taxonomy( 'taxplace', array( 'events' ), $args );

}

/**
 * add new taxonomy
 */
add_action( 'init', 'my_meets_taxonomy' );
function my_meets_taxonomy() {
	$labels = array(
		'name'              => __( 'Категории Встреч' ),
		'singular_name'     => __( 'Категория Встреч' ),
		'search_items'      => __( 'Найти Категорию Встреч' ),
		'all_items'         => __( 'Все' ),
		'parent_item'       => __( 'Родительская Категория' ),
		'parent_item_colon' => __( 'Родительская Категория:' ),
		'edit_item'         => __( 'Редактировать Категорию' ),
		'view item'         => __( 'Посмотреть Категорию' ),
		'update_item'       => __( 'Обновить Категорию' ),
		'add_new_item'      => __( 'Добавить Новую Категорию' ),
		'new_item_name'     => __( 'Новое Имя Категории' ),
		'menu_name'         => __( 'Категории Встреч' )
	);
	$args   = array(
		'label'             => __( 'Категории Встреч' ),
		'labels'            => $labels,
		'public'            => true,
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_in_menu'      => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array(
			'slug' => '' // Текст в ЧПУ. По умолчанию: название таксономии.
			/*'with_front' => false, // Позволяет ссылку добавить к базовому URL.
			'hierarchical' => true, // Использовать (true) или не использовать (false) древовидную структуру ссылок. По умолчанию: false.
			'ep_mask' => EP_NONE // Перезаписывает конечное значение таксономии. По умолчанию: EP_NONE.
		*/
		),
	);
	register_taxonomy( 'taxmeets', array( 'meets' ), $args );

}

/**
 * add custom post
 */
add_action( 'init', 'register_my_post_events' );
function register_my_post_events() {

	$args = array(
		'label'        => __( 'События ' ),
		'labels'       => array(
			'name'               => __( 'События' ),
			// основное название для типа записи
			'singular_name'      => __( 'Событие' ),
			// название для одной записи этого типа
			'add_new'            => __( 'Добавить событие' ),
			// для добавления новой записи
			'add_new_item'       => __( 'Добавление заголовка события' ),
			// заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => __( 'Редактирование события' ),
			// для редактирования типа записи
			'new_item'           => __( 'Новое событие' ),
			// текст новой записи
			'view_item'          => __( 'Смотреть событие' ),
			// для просмотра записи этого типа.
			'search_items'       => __( 'Искать событие' ),
			// для поиска по этим типам записи
			'not_found'          => __( 'Не найдено' ),
			// если в результате поиска ничего не было найдено
			'not_found_in_trash' => __( 'Не найдено в корзине' )
			// если не было найдено в корзине
		),
		'public'       => true,
		'hierarchical' => false,
		'show_ui'      => true,
		'show_in_menu' => true,
		'supports'     => array( 'title', 'editor', 'author', 'thumbnail', 'comments' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'   => array( 'taxplace' ),
		'has_archive'  => true,
		'rewrite'      => array( 'slug' => 'events' ),
		'query_var'    => true
	);
	register_post_type( 'events', $args );

}

/**
 * add custom post
 */
add_action( 'init', 'register_my_post_meets' );
function register_my_post_meets() {

	$args = array(
		'label'        => __( 'Встречи' ),
		'labels'       => array(
			'name'               => __( 'Встречи' ),
			// основное название для типа записи
			'singular_name'      => __( 'Встреча' ),
			// название для одной записи этого типа
			'add_new'            => __( 'Добавить Встречу' ),
			// для добавления новой записи
			'add_new_item'       => __( 'Добавление заголовка Встречи' ),
			// заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => __( 'Редактирование Встречи' ),
			// для редактирования типа записи
			'new_item'           => __( 'Новая Встречи' ),
			// текст новой записи
			'view_item'          => __( 'Смотреть Встречу' ),
			// для просмотра записи этого типа.
			'search_items'       => __( 'Искать Встречи' ),
			// для поиска по этим типам записи
			'not_found'          => __( 'Не найдено' ),
			// если в результате поиска ничего не было найдено
			'not_found_in_trash' => __( 'Не найдено в корзине' )
			// если не было найдено в корзине
		),
		'public'       => true,
		'hierarchical' => false,
		'show_ui'      => true,
		'show_in_menu' => true,
		'supports'     => array(
			'title',
			'editor',
			'author',
			'thumbnail',
			'comments',
			'revisions',
			'page-attributes',
			'post-formats'
		),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'   => array( 'taxmeets' ),
		'has_archive'  => true,
		'rewrite'      => array( 'slug' => 'meets' ),
		'query_var'    => true
	);
	register_post_type( 'meets', $args );

}

/**
 * add metaboxes(custom fields type of events)
 */
add_action( 'add_meta_boxes', 'status_events_meta_boxes' );
function status_events_meta_boxes() {
	add_meta_box(
		'status',
		__( 'Статус' ),
		'status_content',
		'events',
		'side',
		'high'
	);
}

function status_content( $post ) {
	$status = get_post_meta( $post->ID, '_status', true );
	//wp_nonce_field( 'my_action_events', 'my_events' );

	echo "<label><input type='radio' name='status' value='1'";
	checked( $status, 1 );
	echo "/>";
	_e( 'открытое событие' );
	echo "</label><br />";
	//var_dump( $status );
	echo "<label><input type='radio'  name='status' value='2'";
	checked( $status, 2 );
	echo "/>";
	_e( 'только по приглашению' );
	echo "</label>";
}

/**
 * save metaboxes values
 */
add_action( 'save_post', 'status_save_meta_box' );
function status_save_meta_box( $post_id ) {
	$result = trim( strip_tags( $_POST['status'] ) );

	if ( ! isset( $result ) && get_post_type( $post_id ) != 'events' ) {
		return;
	}

	if ( wp_is_post_revision( $post_id ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( isset( $result ) ) {
		//check_admin_referer( 'my_action_events', 'my_events' );
		update_post_meta( $post_id, '_status', $result );
	}
}


/**
 * add metaboxes(custom fields date of events)
 */
add_action( 'add_meta_boxes', 'date_events_meta_boxes' );
function date_events_meta_boxes() {
	add_meta_box(
		'eventdate',
		__( 'Дата' ),
		'date_content',
		'events',
		'side',
		'high'
	);
}

function date_content( $post ) {
	$event_date = get_post_meta( $post->ID, '_eventdate', true );
	//wp_nonce_field( 'my_action_date', 'my_date' );
	echo "<label>";
	_e( 'Выберите дату:' );
	echo "<input type='date' name='eventdate' value='";
	if ( ! empty( $event_date ) ) {
		echo $event_date;
	} else {
		echo date( 'd-m-Y' );
	}
	echo "'  /></label>";
}

/**
 * save metaboxes values
 */
add_action( 'save_post', 'date_save_meta_box' );
function date_save_meta_box( $post_id ) {
	$res = trim( strip_tags( $_POST['eventdate'] ) );

	if ( ! isset( $res ) ) {
		return;
	}

	if ( wp_is_post_revision( $post_id ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( isset( $_POST['eventdate'] ) ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		//check_admin_referer( 'my_action_date', 'my_date' );
		update_post_meta( $post_id, '_eventdate', $res );
	}
}

/**
 * add shortcode
 */
add_shortcode( 'hs', 'my_event_shortcode' );
function my_event_shortcode( $atts, $content = null ) {
	global $post;

	$atts = shortcode_atts( array(
		"hs_status"  => '1',
		"num_events" => '1'
	), $atts );

	if ( absint( $atts['hs_status'] ) > 2 ) {
		$atts['hs_status'] = '1';
	}

	$args_hs = array(
		'post_type'      => 'events',
		'posts_per_page' => sanitize_text_field( $atts['num_events'] ),
		'meta_query'     => array(
			'relation' => 'AND',
			array(
				'key'     => '_status',
				'value'   => trim( strip_tags( $atts['hs_status'] ) ),
				'compare' => '='
			),
			array(
				'key'     => '_eventdate',
				'value'   => date( 'Y-m-d' ),
				'compare' => '>='

			)
		),
		'meta_key'       => '_eventdate',
		'orderby'        => 'meta_value'
	);

	$hs_events = new WP_Query( $args_hs );
	ob_start();
	while ( $hs_events->have_posts() ): $hs_events->the_post();
		$hs_event_date   = get_post_meta( $post->ID, '_eventdate', true );
		$hs_event_status = get_post_meta( $post->ID, '_status', true );
		?>
		<p><a href="<?php the_permalink(); ?>"
		      rel="bookmark"
		      title="<?php the_title_attribute(); ?>Event information">
				<?php the_title(); ?>
			</a>
		</p>
		<?php echo '<p>' . __( 'Дата: ' ) . $hs_event_date . '</p>';
		echo '<p>' . __( 'Тип события: ' ) . $hs_event_status . '</p>';

		//echo '<p>num_event: ' . esc_html( $num_event ) . '</p>';
		//echo '<p>status for display: ' . esc_html( $wid_status ) . '</p>';
	endwhile;
	wp_reset_postdata();

	$output_string = ob_get_contents();
	ob_end_clean();

	return $output_string;
}

/**
 * add widget - "my_event_widget"
 */
add_action( 'widgets_init', 'my_event_register_widgets' );
function my_event_register_widgets() {
	register_widget( 'my_event_widget' );
}

class my_event_widget extends WP_Widget {
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'event_widget_class',
			'description' => __( 'Виджет для вывода предстоящих событий' )
		);
		$this->WP_Widget( 'my_event_widget', 'Event Widget', $widget_ops );
	}

	function form( $instance ) {
		$defaults = array(
			'title'      => __( 'Последние События' ),
			'num_event'  => '5',
			'wid_status' => '1'
		);

		$instance   = wp_parse_args( ( array ) $instance, $defaults );
		$title      = $instance['title'];
		$num_event  = $instance['num_event'];
		$wid_status = $instance['wid_status'];
		?>
		<p><?php _e( 'Заголовок:' ) ?>
			<input class="widefat"
			       name="<?php echo $this->get_field_name( 'title' ); ?>"
			       type="text" value="<?php echo esc_attr( $title ); ?>"/></p>
		<p><?php _e( 'Количество событий:' ) ?>
			<input class="widefat" name="<?php echo $this->get_field_name( 'num_event' ); ?>"
			       type="text" value="<?php echo esc_attr( $num_event ); ?>"/>
		</p>
		<p>Отображать:
		<select class="widefat" name="<?php echo $this->get_field_name( 'wid_status' ); ?>">
			<option value="1" <?php selected( $wid_status, 1 ); ?> >открытые события</option>
			<option value="2" <?php selected( $wid_status, 2 ); ?> >только по приглашению</option>
		</select>
		<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance               = $old_instance;
		$instance['title']      = sanitize_text_field( $new_instance['title'] );
		$instance['num_event']  = trim( strip_tags( $new_instance['num_event'] ) );
		$instance['wid_status'] = trim( strip_tags( $new_instance['wid_status'] ) );

		return $instance;
	}

	function widget( $args, $instance ) {
		global $post;

		extract( $args );
		echo $before_widget;
		$title      = apply_filters( 'widget_title', $instance['title'] );
		$num_event  = ( empty( $instance['num_event'] ) ) ? '&nbsp;' : $instance['num_event'];
		$wid_status = ( empty( $instance['wid_status'] ) ) ? '&nbsp;' : $instance['wid_status'];

		if ( ! empty( $title ) ) {
			echo $before_title . esc_html( $title ) . $after_title;
		}

		$args = array(
			'post_type'      => 'events',
			'posts_per_page' => absint( $num_event ),
			'meta_query'     => array(
				'relation' => 'AND',
				array(
					'key'     => '_status',
					'value'   => trim( strip_tags( $wid_status ) ),
					'compare' => '='
				),
				array(
					'key'     => '_eventdate',
					'value'   => date( 'Y-m-d' ),
					'compare' => '>='

				),
			),
			'meta_key'       => '_eventdate',
			'orderby'        => 'meta_value'
		);

		$new_events = new WP_Query( $args );
		//$new_events->query( $args );

		while ( $new_events->have_posts() ): $new_events->the_post();
			$event_date   = get_post_meta( $post->ID, '_eventdate', true );
			$event_status = get_post_meta( $post->ID, '_status', true );
			?>
			<p><a href="<?php the_permalink(); ?>"
			      rel="bookmark"
			      title="<?php the_title_attribute(); ?>">
					<?php the_title(); ?>
				</a>
			</p>
			<?php echo '<p>';
			_e( 'Дата: ' );
			echo $event_date . '</p>';
			echo '<p>';
			_e( 'Тип события: ' );
			if ( $event_status == 1 ) {
				_e( 'открытое' );
				echo '</p><hr />';
			} else {
				_e( 'закрытое' );
				echo '</p><hr />';
			}

			//echo '<p>num_event: ' . esc_html( $num_event ) . '</p>';
			//echo '<p>status for display: ' . esc_html( $wid_status ) . '</p>';
		endwhile;
		wp_reset_postdata();
		echo $after_widget;
		echo '<br/>';
	}
}